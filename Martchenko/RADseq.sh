#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-10:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load fastqc
fastqc -t 8 -o fastqc_reports *.fastq.gz


################### J_process_radtags_ddRAD1
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca

module load stacks

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR5629/180216_D00423_0293_ACC3AGANXX_barcoded/ddRAD1_AATCCG_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR5629/180216_D00423_0293_ACC3AGANXX_barcoded/ddRAD1_AATCCG_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD1_AATCCG -e sbfI -r -c -q

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR5629/180216_D00423_0293_ACC3AGANXX_barcoded/ddRAD1_ACTGGT_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR5629/180216_D00423_0293_ACC3AGANXX_barcoded/ddRAD1_ACTGGT_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD1_ACTGGT -e sbfI -r -c -q

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR5629/180216_D00423_0293_ACC3AGANXX_barcoded/ddRAD1_TAGTGC_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR5629/180216_D00423_0293_ACC3AGANXX_barcoded/ddRAD1_TAGTGC_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD1_TAGTGC -e sbfI -r -c -q

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR5629/180216_D00423_0293_ACC3AGANXX_barcoded/ddRAD1_TGTCAC_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR5629/180216_D00423_0293_ACC3AGANXX_barcoded/ddRAD1_TGTCAC_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD1_TGTCAC -e sbfI -r -c -q


################### J_process_radtags_ddRAD2
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca

module load stacks

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD2_ATCGAA_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD2_ATCGAA_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD2_ATCGAA -e sbfI -r -c -q

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD2_CCGATG_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD2_CCGATG_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD2_CCGATG -e sbfI -r -c -q

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD2_CGGTTA_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD2_CGGTTA_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD2_CGGTTA -e sbfI -r -c -q

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD2_GTCTAG_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD2_GTCTAG_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD2_GTCTAG -e sbfI -r -c -q


################### J_process_radtags_ddRAD3
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-15:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca

module load stacks

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD3_ATCGAA_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD3_ATCGAA_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD3_ATCGAA -e sbfI -r -c -q

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD3_CCGATG_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD3_CCGATG_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD3_CCGATG -e sbfI -r -c -q

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD3_CGGTTA_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD3_CGGTTA_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD3_CGGTTA -e sbfI -r -c -q

process_radtags -1 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD3_GTCTAG_R1.fastq.gz -2 /home/dmartche/project/raw_data/RADseq/MAR8151/180927_D00423_0325_BCCTDWANXX/ddRAD3_GTCTAG_R2.fastq.gz -o ./samples/ -b ./barcodes_ddRAD3_GTCTAG -e sbfI -r -c -q


################### J_linecounts
#!/bin/bash
#SBATCH --time=0-05:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
for i in *.fastq.gz
do
echo -n $i$'\t' >> linecounts
zcat $i | wc -l >> linecounts
done


################### J_index_genome
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa
bwa index -a bwtsw /home/dmartche/project/mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta


################### J_align_RADseq1
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-30:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa samtools
for f in `ls /home/dmartche/project/RADseq/RADseq1/samples/*.1.fq.gz | cut -f 1 -d'.'` 
do
bwa mem -t 16 mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta ${f}.1.fq.gz ${f}.2.fq.gz > ${f}.sam
# create bam file from sam
samtools view -S -b -@ 16 ${f}.sam > ${f}.bam
# sort the bam file
samtools sort -@ 16 ${f}.bam -o ${f}_sorted.bam
# index the bam file
samtools index ${f}_sorted.bam
done

################### J_align_RADseq2
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-30:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa samtools
for f in `ls /home/dmartche/project/RADseq/RADseq2/samples/*.1.fq.gz | cut -f 1 -d'.'` 
do
bwa mem -t 16 mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta ${f}.1.fq.gz ${f}.2.fq.gz > ${f}.sam
# create bam file from sam
samtools view -S -b -@ 16 ${f}.sam > ${f}.bam
# sort the bam file
samtools sort -@ 16 ${f}.bam -o ${f}_sorted.bam
# index the bam file
samtools index ${f}_sorted.bam
done


################### J_align_RADseq3
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-30:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa samtools
for f in `ls /home/dmartche/project/RADseq/RADseq3/samples/*.1.fq.gz | cut -f 1 -d'.'` 
do
bwa mem -t 16 mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta ${f}.1.fq.gz ${f}.2.fq.gz > ${f}.sam
# create bam file from sam
samtools view -S -b -@ 16 ${f}.sam > ${f}.bam
# sort the bam file
samtools sort -@ 16 ${f}.bam -o ${f}_sorted.bam
# index the bam file
samtools index ${f}_sorted.bam
done


################### J_align_RADseq1rem
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-10:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa samtools
for f in `ls /home/dmartche/project/RADseq/RADseq1/samples/*.1.fq.gz | cut -f 1 -d'.'` 
do
bwa mem -t 16 mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta ${f}.rem.1.fq.gz > ${f}.rem.1.sam
bwa mem -t 16 mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta ${f}.rem.2.fq.gz > ${f}.rem.2.sam
# create bam file from sam
samtools view -S -b -@ 16 ${f}.rem.1.sam > ${f}.rem.1.bam
samtools view -S -b -@ 16 ${f}.rem.2.sam > ${f}.rem.2.bam
# sort the bam file
samtools sort -@ 16 ${f}.rem.1.bam -o ${f}_sorted.rem.1.bam
samtools sort -@ 16 ${f}.rem.2.bam -o ${f}_sorted.rem.2.bam
# index the bam file
samtools index ${f}_sorted.rem.1.bam
samtools index ${f}_sorted.rem.2.bam
done


################### J_align_RADseq2rem
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-10:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa samtools
for f in `ls /home/dmartche/project/RADseq/RADseq2/samples/*.1.fq.gz | cut -f 1 -d'.'` 
do
bwa mem -t 16 mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta ${f}.rem.1.fq.gz > ${f}.rem.1.sam
bwa mem -t 16 mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta ${f}.rem.2.fq.gz > ${f}.rem.2.sam
# create bam file from sam
samtools view -S -b -@ 16 ${f}.rem.1.sam > ${f}.rem.1.bam
samtools view -S -b -@ 16 ${f}.rem.2.sam > ${f}.rem.2.bam
# sort the bam file
samtools sort -@ 16 ${f}.rem.1.bam -o ${f}_sorted.rem.1.bam
samtools sort -@ 16 ${f}.rem.2.bam -o ${f}_sorted.rem.2.bam
# index the bam file
samtools index ${f}_sorted.rem.1.bam
samtools index ${f}_sorted.rem.2.bam
done


################### J_align_RADseq3rem
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-10:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa samtools
for f in `ls /home/dmartche/project/RADseq/RADseq3/samples/*.1.fq.gz | cut -f 1 -d'.'` 
do
bwa mem -t 16 mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta ${f}.rem.1.fq.gz > ${f}.rem.1.sam
bwa mem -t 16 mountain_goat_oreamnos_americans_19May2018_XtcTc.fasta ${f}.rem.2.fq.gz > ${f}.rem.2.sam
# create bam file from sam
samtools view -S -b -@ 16 ${f}.rem.1.sam > ${f}.rem.1.bam
samtools view -S -b -@ 16 ${f}.rem.2.sam > ${f}.rem.2.bam
# sort the bam file
samtools sort -@ 16 ${f}.rem.1.bam -o ${f}_sorted.rem.1.bam
samtools sort -@ 16 ${f}.rem.2.bam -o ${f}_sorted.rem.2.bam
# index the bam file
samtools index ${f}_sorted.rem.1.bam
samtools index ${f}_sorted.rem.2.bam
done


################### J_combine_bams1
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-10:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa samtools
for f in `ls /home/dmartche/project/RADseq/RADseq1/samples/*.1.fq.gz | cut -f 1 -d'.'` 
do
samtools merge ${f}_final.bam ${f}_sorted.bam ${f}_sorted.rem.1.bam ${f}_sorted.rem.2.bam
done


################### J_combine_bams2
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-3:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa samtools
for f in `ls /home/dmartche/project/RADseq/RADseq2/samples/*.1.fq.gz | cut -f 1 -d'.'` 
do
samtools merge ${f}_final.bam ${f}_sorted.bam ${f}_sorted.rem.1.bam ${f}_sorted.rem.2.bam
done


################### J_combine_bams3
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-3:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load bwa samtools
for f in `ls /home/dmartche/project/RADseq/RADseq3/samples/*.1.fq.gz | cut -f 1 -d'.'` 
do
samtools merge ${f}_final.bam ${f}_sorted.bam ${f}_sorted.rem.1.bam ${f}_sorted.rem.2.bam
done


# moving all the merged, aligned, sorted bam files to final_bams directory
mv *_final.bam /home/dmartche/project/RADseq/final_bams
# renaming all the final bam files
for f in `ls /home/dmartche/project/RADseq/finalbams/*_final.bam | cut -f 1 -d'_'` 
do
mv ${f}_final.bam ${f}.bam 
done


# http://catchenlab.life.illinois.edu/stacks/manual/#popmap
# 4.4.2 Reference-aligned Data

################### 
# Redoing with 254 individuals (10 individuals with <100,000 F+R reads excluded) in one population

################### J_gstacksX
#!/bin/bash
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=00-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
gstacks -I ./final_bams/ -M ./pop_map1_254 -O ./gstacksX_out/ -t 16


################### J_popnsX050
#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=00-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
populations -P ./gstacksX050_out/ -M ./pop_map1_254 -r 0.50 --vcf --genepop --fstats --smooth --hwe -t 16


################### J_popnsX065
#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=00-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
populations -P ./gstacksX065_out/ -M ./pop_map1_254 -r 0.65 --vcf --genepop --fstats --smooth --hwe -t 16


################### J_popnsX080
#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=00-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
populations -P ./gstacksX080_out/ -M ./pop_map1_254 -r 0.80 --vcf --genepop --fstats --smooth --hwe -t 16


################### J_popnsX100
#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=00-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
populations -P ./gstacksX100_out/ -M ./pop_map1_254 -r 1.00 --vcf --genepop --fstats --smooth --hwe -t 16


##### Output stats
vcftools --vcf populations.snps.vcf --TsTv-summary


################### Installing fastStructure on graham
# Need to load those exact versions
module load nixpkgs/16.09  gcc/5.4.0
module load gsl/1.16
module load python/2.7.14
module load scipy-stack/2017b

# In /home/dmartche/project :
git clone https://github.com/rajanil/fastStructure
cd ~/proj/fastStructure/vars
python setup.py build_ext --inplace
cd ~/proj/fastStructure
python setup.py build_ext --inplace

# testing with data in /home/dmartche/project/RADseq/fastStructure
python /home/dmartche/project/fastStructure/structure.py -K 2 --input=./convertedSTR --output=./Output_files/K2 --format=str

# run_fast.sh script:
#! /bin/bash
for i in {2..20}
do
python /home/dmartche/project/fastStructure/structure.py -K $i --input=./convertedSTR --output=./Output_files/'K'$i --format=str
done

chmod +x ./run_fast.sh

# J_run_fast
#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=00-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=USERNAME@trentu.ca
module load nixpkgs/16.09  gcc/5.4.0
module load gsl/1.16
module load python/2.7.14
module load scipy-stack/2017b
./run_fast.sh


# In an interactive session:
module load nixpkgs/16.09  gcc/5.4.0
module load gsl/1.16
module load python/2.7.14
module load scipy-stack/2017b
python /home/dmartche/project/fastStructure/chooseK.py --input=/home/dmartche/project/RADseq/fastStructure/Output_files/K
# Model complexity that maximizes marginal likelihood = 4
# Model components used to explain structure in data = 5




