# Script for the mapping of Deer samples in Graham
# Batch 8 comprises 8 samples: BTD_WA1, MD_NM2, Odo_AL3, Odo_MO1_S8, Odo_MX1, Odo_MX2, Odo_NC1, Odo_SC1
# From FastQC to ...

##########################################################################################################
#	SETUP
##########################################################################################################
# path to fastq
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch8

# path to references
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files

# path to analysis directory
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B8

# root
root=/home/ckessler/projects/rrg-shaferab/ckessler



##########################################################################################################
#	PREP 
##########################################################################################################

# going to split up the genome into scaffolds to help with run - do before submitting job
mkdir $analysisdir/mpileup_run
awk 'BEGIN {FS="\t"}; {print $1 FS "0" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.bed
awk 'BEGIN {FS="\t"}; {print $1 FS "1" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.list

# Store the slurms in different directories:
mkdir $analysisdir/fastqc_slurm 
mkdir $analysisdir/trimmed_slurm
mkdir $analysisdir/align_slurm
mkdir $analysisdir/dups_slurm
mkdir $analysisdir/unique_slurm
mkdir $analysisdir/clean_slurm


##########################################################################################################
#	FastQC 
# 16.04, 14:40
##########################################################################################################
module load StdEnv/2016.4
module load fastqc
for f in `ls $fastq/*.fastq.gz`
do

ID=$(echo ${f} | sed "s|$fastq/||" | sed 's/_R[0-9].*//')

echo ${f}
echo ${ID}
sbatch -o $analysisdir/fastqc_slurm/${ID}-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=fastq --wrap="fastqc ${f} -o ${analysisdir}"
sleep 10
done


##########################################################################################################
#	Trimmomatic 
#   16.04, 15:05
##########################################################################################################

for f in $(ls $fastq/*.fastq.gz | sed "s|$fastq/||" | sed 's/_R[0-9].*//' | uniq)
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch8
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B8
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/trimmed_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
sleep 10
done



##########################################################################################################
#	bwa_sort 
#	19.04, 10h
##########################################################################################################

for f in $(ls $fastq/*.fastq.gz |  sed "s|$fastq/||" | sed 's/_R[0-9].*//' | uniq) 
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch8
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B8
root=/home/ckessler/projects/rrg-shaferab/ckessler


sbatch -o $analysisdir/align_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done


##########################################################################################################
#	picard
#	19.04, 16:37
##########################################################################################################

for f in $(ls *.bam | sed 's/.sorted_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B8
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/dups_slurm/${f}-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}
sleep 10
done

##########################################################################################################
#	Unique
# 	20.04, 08:35
##########################################################################################################

for f in $(ls *.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B8
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done


##########################################################################################################
#	realign
# 	20.04
##########################################################################################################

for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B8
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/clean_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}
sleep 10
done


########
# check, should be 20 each
########
for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do 

echo ${f}
ls ${f}* | wc

done


##########################################################################################################
#	Mosdepth
##########################################################################################################
for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B8
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/mosdepth_slurm/${f}-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"

sleep 10
done


##########################################################################################################
#	MultiQC
##########################################################################################################
/home/ckessler/ENV/bin/multiqc -f .

rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B8/multiqc* /Users/camillekessler/Desktop/Dropbox/PhD/Data/multiqc_B8




##########################################################################################################
#	ANGSD
# for all samples, not just this batch
##########################################################################################################
root=/home/ckessler/projects/rrg-shaferab/ckessler
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_ANGSD

ls $root/mapping_B*/*realigned.bam > bam_list 

sbatch -o $analysisdir/ANGSD.out $root/08_ANGSD.sh

#Editing angsd vcf to be compatable with further steps and vcftools we need a new header called vcf_head
#We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
echo "##fileformat=VCFv4.2" > header.txt
module load nixpkgs/16.09
module load intel/2018.3
module load StdEnv/2018.3 
module load tabix

sbatch -o format.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=format --wrap="tabix -r header.txt deer_angsd.vcf.gz > deer_angsd_comp.vcf.gz"



