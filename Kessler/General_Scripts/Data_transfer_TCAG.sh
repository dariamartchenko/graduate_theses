### How to download data from TCAG
# and the scripts used to do it for each batch

ssh ckessler@gra-dtn1.computecanada.ca

# Only do it once
wget https://data-portal.tcag.ca/cli/linux/latest -O tcag-client-1.4.2
# in case of trouble
# chmod u+x tcag-client-1.4.2
# chmod +x tcag-client-1.4.2

# transfer ID changes each time
cd projects/rrg-shaferab/ckessler/

/home/ckessler/projects/rrg-shaferab/ckessler/tcag-client-1.4.2 download -p V1HVJ5A:/ 
  
## username and password for TCAG
username: camillekessler@trentu.ca
Password: [hidden]

# other downloads
## Batch 9
cd projects/rrg-shaferab/ckessler/
/home/ckessler/projects/rrg-shaferab/ckessler/tcag-client-1.4.2 download -p T51RRHG:/ 
mv T51RRHG DATA_FASTQ_Batch9
cd DATA_FASTQ_Batch9
mv WOO17040.20210510/210504_E00389_0520_AHGFM5CCX2/* .

## Batch 10
cd projects/rrg-shaferab/ckessler/
/home/ckessler/projects/rrg-shaferab/ckessler/tcag-client-1.4.2 download -p LTTRMQ9:/ 
mv LTTRMQ9 DATA_FASTQ_Batch10
cd DATA_FASTQ_Batch10
mv WOO17308.20210604/210601_E00389_0521_AHGF37CCX2/* .

