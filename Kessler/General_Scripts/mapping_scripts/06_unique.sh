#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=unique
#SBATCH --cpus-per-task=30
#SBATCH --mem=12G
#SBATCH --time=0-01:00 # time (DD-HH:MM)

module load StdEnv/2016.4
module load samtools
module load sambamba
echo ${1}
${3}/sambamba-0.7.0-linux-static view \
--nthreads=30 --with-header --format=bam --show-progress \
-F "mapping_quality >= 1 and not (unmapped or secondary_alignment) and not ([XA] != null or [SA] != null)" \
${2}/${1}.deduped_reads.bam \
-o ${2}/${1}.unique_reads.bam &&
${3}/sambamba-0.7.0-linux-static flagstat \
--nthreads=30 \
${2}/${1}.unique_reads.bam \
> ${2}/${1}.unique_reads.flagstat