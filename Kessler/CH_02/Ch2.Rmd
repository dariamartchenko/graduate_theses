---
# Summary of Chapter 2

title: "PhD Chapter 2"
author: C.Kessler
date: "`r format(Sys.time(), '%d %B %Y')`"
output:
  html_document:
    theme: cosmo
    highlight: kate
    toc: true
    toc_depth: 4
    toc_float: true
editor_options: 
  chunk_output_type: console
---



# About

Our study species are the white-tailed deer ( _Odocoileus virginianus_ , hereafter WTD) and the mule deer ( _O. hemionus_ , hereafter MD). Both species are different phenotypically and in their habitat use, they however are sympatric in a wide area of their respective range. 

Both species are highly successful on the continent and previous studies showed high resilience. Indeed, in several reintroduced populations, which generally leaves genetic footprints, no sign of bottleneck was found.


**Questions:**

How and why did those species become so predominant?

**Hypotheses:**

Overall, we expect to find a high genetic diversity with low substructure, a sign of a large and healthy population.

We believe our island populations will show no sign of bottleneck, as shown in previous studies.

However, we expect to find signs of expanding population across the whole range of both species


# Sampling

We plan to sample to whole northern American range of both species for a total of x : x WTD and x MD. Additionally, we will sample three island populations: x samples from the Florida Keys, x from Anticosti island and x from ST-Pierre et Miquelon.


![Fig.1 Species sampling](/Users/camillekessler/Desktop/Dropbox/PhD/Results/WTD_map_colour_sp.png)

**TODO**

Select a subset of island samples


# Analyses

Heterozygosity
Fst
D
π
Tajima's D
ROH
πn/ πs
SFS dadi fastsimcoal
Treemix https://bitbucket.org/nygcresearch/treemix/wiki/Home

# Methods

Tissue samples were sent by ....

DNA was extracted using Qiagen DNEasy kit? following manufacturer's instructions

DNA was sent to The Centre for Applied Genomics for sequencing on an Illumina Hiseq X.















